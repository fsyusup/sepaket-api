<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

$router->group(['namespace' => 'API'], function () use ($router) {


    $router->group(['prefix' => 'auth'], function () use ($router) {
        // Front Route
        $router->post('/login', 'AuthController@login');
        $router->post('/login-google', 'AuthController@loginGoogle');
        $router->post('/register', 'AuthController@register');
    });

    $router->group(['prefix' => 'vendor', 'namespace' => 'Vendor'], function () use ($router) {

        // Service
        $router->get('/service', 'ServiceController@index');
        $router->get('/service/{id}', 'ServiceController@detail');
        $router->post('/service', 'ServiceController@add');
        $router->put('/service/{id}', 'ServiceController@update');
        $router->delete('/service/{id}', 'ServiceController@delete');
    });

    $router->group(['namespace' => 'Master'], function () use ($router) {

        // Service
        $router->get('/location', 'LocationController@index');
        $router->get('/location/{id}', 'LocationController@detail');
        $router->post('/location', 'LocationController@add');
        $router->put('/location/{id}', 'LocationController@update');
        $router->delete('/location/{id}', 'LocationController@delete');

        // Category
        $router->get('/category', 'CategoryController@index');
        $router->get('/category/{id}', 'CategoryController@detail');
        $router->post('/category', 'CategoryController@add');
        $router->put('/category/{id}', 'CategoryController@update');
        $router->delete('/category/{id}', 'CategoryController@delete');

        // Vendor
        $router->get('/vendor', 'VendorController@index');
        $router->get('/vendor/{id}', 'VendorController@detail');
        $router->post('/vendor', 'VendorController@add');
        $router->put('/vendor/{id}', 'VendorController@update');
        $router->delete('/vendor/{id}', 'VendorController@delete');
    });


     $router->group(['namespace' => 'Front'], function () use ($router) {

        // Service
        $router->get('/service', 'ServiceController@index');
        $router->get('/service/{slug}', 'ServiceController@detail');
    });
});
