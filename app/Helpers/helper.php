<?php

use App\Models\City;
use App\Models\Menu;
use App\Models\Bulletin;
use App\Models\Language;
use App\Models\MasterTitle;
use App\Models\NewsExternal;
use App\Models\EventCategory;
use App\Models\CategoryCollection;
use App\Models\FavoriteCollection;
use Illuminate\Support\Facades\Session;
use Firebase\JWT\JWT;


if (!function_exists('random_string')) {
    function random_string($length, $upperCase = true, $lowerCase = true, $numeric = true)
    {
        $characters = '';
        if ($numeric) {
            $characters .= "0123456789";
        }
        if ($lowerCase) {
            $characters .= "abcdefghijklmnopqrstuvwxyz";
        }
        if ($upperCase) {
            $characters .= "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        }
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}


if (!function_exists('upload_file')) {
    function upload_file($file, $path, $allowed_ext = [])
    {
        $fileExt = $file->getClientOriginalName();
        $arr_ext = explode('.', $fileExt);
        $real_ext = $arr_ext[count($arr_ext) - 1];

        $allowed_ext =  array("png", "jpg", "jpeg", "svg");
        if (!empty($allowed_ext)) {
            $allowed_ext = $allowed_ext;
        }

        if (!in_array($real_ext, $allowed_ext)) {
            return redirect('/us/')->withInput()->withErrors([
                'message' => 'Upload gagal. Format file tidak sesuai. Mohon dicoba lagi.'
            ]);
        }

        $fileSize = $file->getClientSize();
        if ($fileSize == 0) {
            return redirect('/us/')->withInput()->withErrors([
                'message' => 'Upload gagal. Ukuran file tidak valid.'
            ]);
        }

        $fileName = date('YmdHis') . random_string(30) . '.' . $real_ext;
        $oriPath = $file->getPathName();
        $destPath = base_path() . '/public/files' . $path . $fileName;
        $copy = \File::copy($oriPath, $destPath);
        return '/files' . $path . $fileName;
    }
}

if (!function_exists('create_slug')) {
    function create_slug($string)
    {
        //Lower case everything
        $string = strtolower($string);
        //Make alphanumeric (removes all other characters)
        $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
        //Clean up multiple dashes or whitespaces
        $string = preg_replace("/[\s-]+/", " ", $string);
        //Convert whitespaces and underscore to dash
        $string = preg_replace("/[\s_]/", "-", $string);
        return $string . '-' . date('YmdHis');
    }
}

if (!function_exists('create_slug_service')) {
    function create_slug_service($string)
    {
        //Lower case everything
        $string = strtolower($string);
        //Make alphanumeric (removes all other characters)
        $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
        //Clean up multiple dashes or whitespaces
        $string = preg_replace("/[\s-]+/", " ", $string);
        //Convert whitespaces and underscore to dash
        $string = preg_replace("/[\s_]/", "-", $string);
        if (strpos($string, 'jasa') === false) {
            return 'jasa-' . $string . '-' . date('YmdHis');
        } else {
            return $string . '-' . date('YmdHis');
        }
    }
}

if (!function_exists('tgl_indo')) {
    function date_indo($tgl)
    {
        $ubah = gmdate($tgl, time() + 60 * 60 * 8);
        $pecah = explode("-", $ubah);
        $tanggal = $pecah[2];
        $bulan = bulan($pecah[1]);
        $tahun = $pecah[0];
        return $tanggal . ' ' . $bulan . ' ' . $tahun;
    }
}

if (!function_exists('bulan')) {
    function bulan($bln)
    {
        switch ($bln) {
            case 1:
                return "Januari";
                break;
            case 2:
                return "Februari";
                break;
            case 3:
                return "Maret";
                break;
            case 4:
                return "April";
                break;
            case 5:
                return "Mei";
                break;
            case 6:
                return "Juni";
                break;
            case 7:
                return "Juli";
                break;
            case 8:
                return "Agustus";
                break;
            case 9:
                return "September";
                break;
            case 10:
                return "Oktober";
                break;
            case 11:
                return "November";
                break;
            case 12:
                return "Desember";
                break;
        }
    }
}

//Format Shortdate
if (!function_exists('shortdate_indo')) {
    function shortdate_indo($tgl)
    {
        $ubah = gmdate($tgl, time() + 60 * 60 * 8);
        $pecah = explode("-", $ubah);
        $tanggal = $pecah[2];
        $bulan = short_bulan($pecah[1]);
        $tahun = $pecah[0];
        return $tanggal . '/' . $bulan . '/' . $tahun;
    }
}

if (!function_exists('short_bulan')) {
    function short_bulan($bln)
    {
        switch ($bln) {
            case 1:
                return "01";
                break;
            case 2:
                return "02";
                break;
            case 3:
                return "03";
                break;
            case 4:
                return "04";
                break;
            case 5:
                return "05";
                break;
            case 6:
                return "06";
                break;
            case 7:
                return "07";
                break;
            case 8:
                return "08";
                break;
            case 9:
                return "09";
                break;
            case 10:
                return "10";
                break;
            case 11:
                return "11";
                break;
            case 12:
                return "12";
                break;
        }
    }
}

//Format Medium date
if (!function_exists('mediumdate_indo')) {
    function mediumdate_indo($tgl)
    {
        $ubah = gmdate($tgl, time() + 60 * 60 * 8);
        $pecah = explode("-", $ubah);
        $tanggal = $pecah[2];
        $bulan = medium_bulan($pecah[1]);
        $tahun = $pecah[0];
        return $tanggal . '-' . $bulan . '-' . $tahun;
    }
}

if (!function_exists('medium_bulan')) {
    function medium_bulan($bln)
    {
        switch ($bln) {
            case 1:
                return "Jan";
                break;
            case 2:
                return "Feb";
                break;
            case 3:
                return "Mar";
                break;
            case 4:
                return "Apr";
                break;
            case 5:
                return "Mei";
                break;
            case 6:
                return "Jun";
                break;
            case 7:
                return "Jul";
                break;
            case 8:
                return "Ags";
                break;
            case 9:
                return "Sep";
                break;
            case 10:
                return "Okt";
                break;
            case 11:
                return "Nov";
                break;
            case 12:
                return "Des";
                break;
        }
    }
}

//Long date indo Format
if (!function_exists('longdate_indo')) {
    function longdate_indo($tanggal)
    {
        $ubah = gmdate($tanggal, time() + 60 * 60 * 8);
        $pecah = explode("-", $ubah);
        $tgl = $pecah[2];
        $bln = $pecah[1];
        $thn = $pecah[0];
        $bulan = bulan($pecah[1]);

        $nama = date("l", mktime(0, 0, 0, $bln, $tgl, $thn));
        $nama_hari = "";
        if ($nama == "Sunday") {
            $nama_hari = "Minggu";
        } else if ($nama == "Monday") {
            $nama_hari = "Senin";
        } else if ($nama == "Tuesday") {
            $nama_hari = "Selasa";
        } else if ($nama == "Wednesday") {
            $nama_hari = "Rabu";
        } else if ($nama == "Thursday") {
            $nama_hari = "Kamis";
        } else if ($nama == "Friday") {
            $nama_hari = "Jumat";
        } else if ($nama == "Saturday") {
            $nama_hari = "Sabtu";
        }
        return $nama_hari . ',' . $tgl . ' ' . $bulan . ' ' . $thn;
    }
}


if (!function_exists('longdate')) {
    function longdate($tanggal)
    {
        $ubah = gmdate($tanggal, time() + 60 * 60 * 8);
        $pecah = explode("-", $ubah);
        $tgl = $pecah[2];
        $bln = $pecah[1];
        $thn = $pecah[0];
        $bulan = bulan($pecah[1]);

        $nama = date("l", mktime(0, 0, 0, $bln, $tgl, $thn));
        $nama_hari = "";
        if ($nama == "Sunday") {
            $nama_hari = "Minggu";
        } else if ($nama == "Monday") {
            $nama_hari = "Senin";
        } else if ($nama == "Tuesday") {
            $nama_hari = "Selasa";
        } else if ($nama == "Wednesday") {
            $nama_hari = "Rabu";
        } else if ($nama == "Thursday") {
            $nama_hari = "Kamis";
        } else if ($nama == "Friday") {
            $nama_hari = "Jumat";
        } else if ($nama == "Saturday") {
            $nama_hari = "Sabtu";
        }
        return $tgl . ' ' . $bulan . ' ' . $thn;
    }
}

if (!function_exists('responseFailed')) {
    function responseFailed($data = [], $message = 'FAILED', $code = 200)
    {
        return response()->json(
            [
                'status' => false,
                // 'code' => $code,
                'data' => $data,
                'message' => $message
            ],
            $code
        );
    }
}

if (!function_exists('responseSuccess')) {
    function responseSuccess($data = [], $message = 'OK', $code = 200)
    {
        return response()->json(
            [
                'status' => true,
                // 'code' => $code,
                'data' => $data,
                'message' => $message
            ],
            $code
        );
    }
}


if (! function_exists('extractToken')) {
    function extractToken($request)
    {
        $token = $request->header('s-token') ?? $request->header('Authorization');
        $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
        return $credentials->sub;
    }
}



if (! function_exists('randomString')) {
    function randomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}


if (! function_exists('cdn')) {
    function cdn($path) {
       return url($path);
    }
}





// if (! function_exists('upload_file')) {
//     function upload_file($file, $path, $allowed_ext = []){
//         $fileExt = $file->getClientOriginalName();
//         $arr_ext = explode('.', $fileExt);
//         $real_ext = $arr_ext[count($arr_ext) - 1];
        
//         $allowed_ext =  array("png", "jpg", "jpeg", "svg");
//         if(!empty($allowed_ext)){
//             $allowed_ext = $allowed_ext;
//         }
        
//         if (!in_array($real_ext, $allowed_ext)) {
//             return redirect('/us/')->withInput()->withErrors([
//                 'message' => 'Upload gagal. Format file tidak sesuai. Mohon dicoba lagi.'
//                 ]);
//             }
            
//             $fileSize = $file->getClientSize();
//             if($fileSize == 0){
//                 return redirect('/us/')->withInput()->withErrors([
//                     'message' => 'Upload gagal. Ukuran file tidak valid.'
//                     ]);
//                 }
                
//                 $fileName = date('YmdHis').randomString(30).'.'.$real_ext;
//                 $oriPath = $file->getPathName();
//                 $destPath = base_path().$path.$fileName;
//                 $copy = File::copy($oriPath, $destPath);
//                 return ltrim($path, '/public/').$fileName;
//             }
//         }
