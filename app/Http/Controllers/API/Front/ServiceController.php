<?php

namespace App\Http\Controllers\API\Front;

use App\Models\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServiceController extends Controller
{
    public function index(Request $request)
    {
        $data = $request->all();

        $category = !in_array($request->input('category'), ['all', null]) ? $data['category'] : null;
        $location = !in_array($request->input('location'), ['all', null]) ? $data['location'] : null;
        $minPrice = isset($data['min_price']) ? (int) $data['min_price'] : null;
        $maxPrice = isset($data['max_price']) ? (int) $data['max_price'] : null;
        $keyword = isset($data['q']) ? $data['q'] : null;
        $vendorType = $data['vendor_type'] ?? '';
        $serviceType = $data['service_type'] ?? '';
        $sort = $data['sort'] ?? '';


        $services = Service::with('vendor')->with('location')
            ->orderBy('created_at', 'desc')
            ->when($keyword, function ($query, $keyword) {
                return $query->where('name', 'LIKE', "%$keyword%");
            })
            ->when($vendorType, function ($query, $vendorType) {
                return $query->where('vendor_type', $vendorType);
            })
            ->when($serviceType, function ($query, $serviceType) {
                return $query->where('service_type', $serviceType);
            })
            ->when($category, function ($query, $category) {
                return $query->where('category_slug', $category);
            })
            ->when($location, function ($query, $location) {
                return $query->where('location_type', 'all')->orWhereIn('location_id',  $location);
            })
            ->when($minPrice, function ($query, $minPrice) {
                return $query->where('starting_price', '>=', $minPrice);
            })
            ->when($maxPrice, function ($query, $maxPrice) {
                return $query->where('starting_price', '<=', $maxPrice);
            })
            ->when($sort, function ($query, $sort) {
                switch ($sort) {
                    case 'low_price':
                        return $query->orderBy('starting_price', 'ASC');
                        break;
                    case 'new_post':
                        return $query->orderBy('created_at', 'DESC');
                        break;
                    case 'high_rate':
                        return $query->orderBy('avg_rating', 'DESC');
                        break;
                }
            })
            ->paginate(10)
            ->appends($request->query());

        return responseSuccess($services);
    }

    public function detail($slug)
    {
        $res = Service::where('slug', $slug)->with('vendor')->with('location')->first();

        $newImage = [];
        foreach ($res['images'] ?? [] as $key => $value) {
            $newImage[] = cdn($value);
        }

        if(!empty($newImage)){
            $res->images = $newImage;
        }

        if(!empty($res)){
            return responseSuccess($res);
        }

        return responseFailed([], 'Data not found!');
    }
}
