<?php

namespace App\Http\Controllers\API\Master;

use App\Models\Service;
use App\Models\Location;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class LocationController extends Controller
{
    public function index(Request $req)
    {
        $data = $req->all();
        try {
            $res = Location::get();
            return responseSuccess($res);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public function detail(Request $req, $id)
    {
        $data = $req->all();
        try {
            $res = Location::where('location_id', $id)->first();
            return responseSuccess($res);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public function add(Request $req)
    {
        $data = $req->all();
        try {

            $validator = Validator::make($req->all(), [
                'name' => 'required'
            ]);

            if ($validator->fails()) {
                return responseFailed($data, $validator->errors()->first());
            }

            $formData = [
                'name' => $data['name']
            ];

            $res = Location::insert($formData);

            return responseSuccess($res);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public function update(Request $req, $id)
    {
        $data = $req->all();
        try {

            $formData = [
                'name' => $data['name']
            ];
            $res = Location::where('location_id', $id)->update($formData);

            if ($res) {
                return responseSuccess($res);
            }
            return responseFailed($formData);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public function delete(Request $req, $id)
    {
        $data = $req->all();
        try {
            $res = Location::where('location_id', $id)->delete();

            if ($res) {
                return responseSuccess($res);
            }
            return responseFailed($data);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }
}
