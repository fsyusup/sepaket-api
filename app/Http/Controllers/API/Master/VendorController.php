<?php

namespace App\Http\Controllers\API\Master;

use App\Models\Vendor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class VendorController extends Controller
{
    public function index(Request $req)
    {
        $data = $req->all();
        try {
            $res = Vendor::with('user')->get();
            return responseSuccess($res);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public function detail(Request $req, $id)
    {
        $data = $req->all();
        try {
            $res = Vendor::with('user')->where('vendor_id', $id)->first();
            return responseSuccess($res);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public function add(Request $req)
    {
        $data = $req->all();
        try {

            $validator = Validator::make($req->all(), [
                'name' => 'required',
                'description' => 'required',
                'location_id' => 'required',
                'user_id' => 'required'
            ]);

            if ($validator->fails()) {
                return responseFailed($data, $validator->errors()->first());
            }

            $formData = [
                'name' => $data['name'],
                'description' => $data['description'],
                'location_id' => $data['location_id'],
                'user_id' => $data['user_id'],
            ];

            $res = Vendor::insert($formData);

            return responseSuccess($res);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public function update(Request $req, $id)
    {
        $data = $req->all();
        try {

            $validator = Validator::make($req->all(), [
                'name' => 'required',
                'description' => 'required'
            ]);

            if ($validator->fails()) {
                return responseFailed($data, $validator->errors()->first());
            }

            $formData = [
                'name' => $data['name'],
                'description' => $data['description'],
                'location_id' => $data['location_id'],
                'user_id' => $data['user_id'],
            ];

            $res = Vendor::where('vendor_id', $id)->update($formData);

            if ($res) {
                return responseSuccess($res);
            }
            return responseFailed($formData);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public function delete(Request $req, $id)
    {
        $data = $req->all();
        try {
            $res = Vendor::where('vendor_id', $id)->delete();

            if ($res) {
                return responseSuccess($res);
            }
            return responseFailed($data);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }
}
