<?php

namespace App\Http\Controllers\API;

use App\Models\User;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function jwt($user)
    {
        $payload = [
            'iss' => "user-jwt", // Issuer of the token
            'sub' => $user->user_id, // Subject of the token
            'iat' => time(), // Time when JWT was issued. 
            'exp' => time() + 60 * 60 * 12 * 365 // Expiration time
        ];

        return JWT::encode($payload, env('JWT_SECRET'));
    }

    public function login(Request $req)
    {

        $data = $req->all();

        $validator = Validator::make($req->all(), [
            'email' => 'required|email|exists:user|min:1|max:35',
            'password' => 'required|min:6',
        ]);

        $user = User::where('email', $req->input('email'))->first();

        if (!$user || $validator->fails()) {
            return responseFailed($data, $validator->errors()->first());
        }

        if (Hash::check($req->input('password'), $user['password'])) {

            $jwt = $this->jwt($user);
            $ttl = 1;

            Redis::set('auth:' . $jwt, $jwt, 'EX', $ttl);
            return responseSuccess(['token' => $this->jwt($user)], 'Login berhasil!');
        }

        return responseFailed($data, $validator->errors()->first());
    }

    public function loginGoogle(Request $req)
    {
        $data = $req->all();
        $validator = Validator::make($req->all(), [
            'email' => 'required|email|exists:user|min:1|max:35',
            'token' => 'required',
        ]);

        $user = User::where('email', $req->input('email'))
            ->where('google_token', $req->input('token'))
            ->first();

        if ($user) {
            $jwt = $this->jwt($user);
            $ttl = 1;

            Redis::set('auth:' . $jwt, $jwt, 'EX', $ttl);
            return responseSuccess(['token' => $this->jwt($user)], 'Login berhasil!');
        }

        return responseFailed([], 'LOGIN_GOOGLE_NOT_FOUND');
    }

    public function register(Request $req)
    {

        $data = $req->all();

        $validator = Validator::make($req->all(), [
            'email' => 'required|unique:user|max:35',
            'password' => 'required|min:6',
            'full_name' => 'required',
            'phone_number' => 'required'
        ]);

        if ($validator->fails()) {
            return responseFailed($data, $validator->errors()->first());
        }

        try {
            $dataUser = [
                'email' => $req->email,
                'phone_number' => $req->phone_number,
                'password' => app('hash')->make($req->password),
                'full_name' => $req->full_name
            ];

            $user = User::insertGetId($dataUser);

            if ($user) {
                return responseSuccess($req, 'Registrasi berhasil!');
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 'USER_REGISTRATION_FAILED'], 409);
        }
    }
}
