<?php

namespace App\Http\Controllers\API\Vendor;

use App\Models\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ServiceController extends Controller
{
    public function index(Request $request)
    {
        $data = $request->all();

        $category = !in_array($request->input('category'), ['all', null]) ? $data['category'] : null;
        $location = !in_array($request->input('location'), ['all', null]) ? $data['location'] : null;
        $minPrice = isset($data['min_price']) ? (int) $data['min_price'] : null;
        $maxPrice = isset($data['max_price']) ? (int) $data['max_price'] : null;
        $keyword = isset($data['q']) ? $data['q'] : null;
        $vendorType = $data['vendor_type'] ?? '';
        $serviceType = $data['service_type'] ?? '';
        $sort = $data['sort'] ?? '';


        $services = Service::with('vendor')->with('location')
            ->orderBy('created_at', 'desc')
            ->when($keyword, function ($query, $keyword) {
                return $query->where('name', 'LIKE', "%$keyword%");
            })
            ->when($vendorType, function ($query, $vendorType) {
                return $query->where('vendor_type', $vendorType);
            })
            ->when($serviceType, function ($query, $serviceType) {
                return $query->where('service_type', $serviceType);
            })
            ->when($category, function ($query, $category) {
                return $query->where('category_slug', $category);
            })
            ->when($location, function ($query, $location) {
                return $query->where('location_type', 'all')->orWhereIn('location_id',  $location);
            })
            ->when($minPrice, function ($query, $minPrice) {
                return $query->where('starting_price', '>=', $minPrice);
            })
            ->when($maxPrice, function ($query, $maxPrice) {
                return $query->where('starting_price', '<=', $maxPrice);
            })
            ->when($sort, function ($query, $sort) {
                switch ($sort) {
                    case 'low_price':
                        return $query->orderBy('starting_price', 'ASC');
                        break;
                    case 'new_post':
                        return $query->orderBy('created_at', 'DESC');
                        break;
                    case 'high_rate':
                        return $query->orderBy('avg_rating', 'DESC');
                        break;
                }
            })
            ->paginate(10)
            ->appends($request->query());

        return responseSuccess($services);
    }

    public function detail($id)
    {
        $res = Service::where('slug', $id)->orWhere('service_id', $id)->with('vendor')->with('location')->first();

        $newImage = [];
        foreach ($res['images'] ?? [] as $key => $value) {
            $newImage[] = cdn($value);
        }

        if(!empty($newImage)){
            $res->images = $newImage;
        }

        if(!empty($res)){
            return responseSuccess($res);
        }

        return responseFailed([], 'Data not found!');
    }

    public function recent(Request $request)
    {
        $data = $request->all();

        $category = !in_array($request->input('category'), ['all', null]) ? $data['category'] : null;
        $location = !in_array($request->input('location'), ['all', null]) ? $data['location'] : null;
        $minPrice = isset($data['min_price']) ? (int) $data['min_price'] : null;
        $maxPrice = isset($data['max_price']) ? (int) $data['max_price'] : null;
        $keyword = isset($data['q']) ? $data['q'] : null;

        $services = Service::where('is_delete', false)
            ->with('vendor')
            ->with('location')
            ->orderBy('created_at', 'asc')
            ->where('status', 'publish')
            ->limit(8)->get();

        return responseSuccess($services);
    }

    public function add(Request $req)
    {

        $data = $req->all();
        $validator = Validator::make($req->all(), [
            'name' => 'required',
            'description' => 'required',
            'vendor_id' => 'required',
            'type' => 'required',
            'location_type' => 'required'
        ]);

        if ($validator->fails()) {
            return responseFailed($data, $validator->errors()->first());
        }

        $data = $req->all();
        $data_image = [];
        foreach ($data['images'] ?? [] as $image) {
            $data_image[] = upload_file($image, env('UPLOAD_PATH') . "/service/", ['svg', 'png', 'jpg', 'jpeg']);
        }
        $formData = [
            'vendor_id' => $data['vendor_id'], 
            'slug' => create_slug_service($data['name']),
            'name' => $data['name'],
            'description' => $data['description'],
            'type' => $data['type'],
            'location_type' => $data['location_type'],
            // 'images' => $data_image,
            // 'reviews' => 0,
            // 'total_review' => 0,
            // 'avg_rating' => 0,
            // 'total_order' => 0,
            'created_at' => date('Y-m-d H:i:s')
        ];

        // check location type
        if ($data['location_type'] == 'custom') {
            $formData['location_id'] = $data['location_id'];
        }

        // check service type
        if ($data['type'] == 'package') {
            $formData['packages'] = $data['packages'];
            foreach ($data['packages'] as $package) {
                // check price type
                $selectPrice = $package['package_price_type'] == 'range' ? 'package_min_price' : 'package_price';
                if (!isset($formData['starting_price'])) {
                    $formData['starting_price'] = $package[$selectPrice];
                } else {
                    $formData['starting_price'] = $package[$selectPrice] < $formData['starting_price'] ? (int) $package[$selectPrice] : (int) $formData['starting_price'];
                }
            }
        } else {
            $selectPrice = $data['price_type'] == 'range' ? 'min_price' : 'min_price';
            if (!isset($formData['starting_price'])) {
                $formData['starting_price'] = (int) $data[$selectPrice];
            } else {
                $formData['starting_price'] = $data[$selectPrice] < $formData['starting_price'] ? (int) $data[$selectPrice] : (int) $formData['starting_price'];
            }

            if ($data['price_type'] == 'range') {
                $formData['min_price'] = (int) $data['min_price'];
                $formData['max_price'] = (int) $data['max_price'];
            } else {
                $formData['min_price'] = (int) $data['min_price'];
            }
        }


        Service::insert($formData);

        return response()->json(['status' => true]);
    }

    public function update(Request $request, $id)
    {

        $data = $request->all();
        $data_image = [];
        foreach ($data['images'] ?? [] as $image) {
            $data_image[] = upload_file($image, env('UPLOAD_PATH') . "/service/", ['svg', 'png', 'jpg', 'jpeg']);
        }
        $formData = [
            'name' => $data['name'],
            'type' => $data['type'],
            'location_type' => $data['location_type'],
            'description' => $data['description']
        ];

        // check location type
        if ($data['location_type'] == 'custom') {
            $formData['location_id'] = $data['location_id'];
        }

        // check service type
        if ($data['type'] == 'package') {
            $formData['packages'] = $data['packages'];
            foreach ($data['packages'] as $package) {
                // check price type
                $selectPrice = $package['package_price_type'] == 'range' ? 'package_min_price' : 'package_price';
                if (!isset($formData['starting_price'])) {
                    $formData['starting_price'] = $package[$selectPrice];
                } else {
                    $formData['starting_price'] = $package[$selectPrice] < $formData['starting_price'] ? (int) $package[$selectPrice] : (int) $formData['starting_price'];
                }
            }
        } else {
            $selectPrice = $data['price_type'] == 'range' ? 'min_price' : 'min_price';
            if (!isset($formData['starting_price'])) {
                $formData['starting_price'] = (int) $data[$selectPrice];
            } else {
                $formData['starting_price'] = $data[$selectPrice] < $formData['starting_price'] ? (int) $data[$selectPrice] : (int) $formData['starting_price'];
            }

            if ($data['price_type'] == 'range') {
                $formData['min_price'] = (int) $data['min_price'];
                $formData['max_price'] = (int) $data['max_price'];
            } else {
                $formData['min_price'] = (int) $data['min_price'];
            }
        }

        Service::where('service_id', $id)->update($formData);

        return response()->json(['status' => true]);
    }

    public function delete(Request $req, $id)
    {
        $data = $req->all();
        try {
            $res = Service::where('service_id', $id)->delete();

            if ($res) {
                return responseSuccess($res);
            }
            return responseFailed($data);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }
}
