<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableService extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service', function (Blueprint $table) {
            $table->bigIncrements('service_id');
            $table->string('name', 50);
            $table->text('slug');
            $table->enum('type',['regular','package']);
            $table->enum('location_type',['all','custom']);
            $table->text('description');
            $table->integer('starting_price')->nullable();
            $table->enum('price_type',['fixed','range']);
            $table->integer('min_price')->nullable();
            $table->integer('max_price')->nullable();
            $table->string('location_id', 35);
            $table->integer('vendor_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->softDeletes($column = 'deleted_at', $precision = 0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_service');
    }
}
