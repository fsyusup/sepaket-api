<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_request', function (Blueprint $table) {
            $table->bigIncrements('service_request_id');
            $table->string('name');
            $table->date('deadline');
            $table->enum('code',['REG','EXP','BID']);
            $table->text('description');
            $table->datetime('date');
            $table->datetime('date_deal');
            $table->enum('status', ['pending','ongoing','done']);
            $table->integer('min_price');
            $table->integer('max_price');
            $table->integer('fixed_price');
            $table->integer('sla'); //in hour
            $table->integer('category_id');
            $table->integer('vendor_id');
            $table->integer('user_id');
            $table->softDeletes($column = 'deleted_at', $precision = 0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_request');
    }
}
